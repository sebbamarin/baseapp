﻿using Microsoft.EntityFrameworkCore;
using Redjar.BaseApp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Redjar.BaseApp.Application.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly ApplicationDbContext applicationDbContext;

        public GenericRepository(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }
        public void Delete(T entity)
        {
            applicationDbContext.Entry(entity).State = EntityState.Deleted;
        }

        public void Delete(Guid id)
        {
            var entry = applicationDbContext.Set<T>().Find(id);
            applicationDbContext.Entry(entry).State = EntityState.Deleted;
        }

        public T Find(Guid id)
        {
            return applicationDbContext.Set<T>().Find(id);
            //Todo: Desactivar tracking de la entidad
        }

        public void Insert(T entity)
        {
            applicationDbContext.Set<T>().Add(entity);
        }

        public IQueryable<T> List(Expression<Func<T, bool>> expression)
        {
            return applicationDbContext.Set<T>().AsNoTracking().Where(expression);
        }

        public void Patch(T entity, Guid id)
        {
            throw new NotImplementedException(); //TODO
        }

        public void Update(T entity)
        {
            applicationDbContext.Entry(entity).State = EntityState.Modified;
        }
    }
}

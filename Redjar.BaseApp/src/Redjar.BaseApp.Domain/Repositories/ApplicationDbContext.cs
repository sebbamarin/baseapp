﻿using Microsoft.EntityFrameworkCore;
using Redjar.BaseApp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Redjar.BaseApp.Domain.Repositories
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
        DbSet<Person> People { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}

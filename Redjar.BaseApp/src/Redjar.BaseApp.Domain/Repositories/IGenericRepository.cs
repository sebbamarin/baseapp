﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Redjar.BaseApp.Domain.Repositories
{
    public interface IGenericRepository<T> where T : class
    {
        //Add
        void Insert(T entity);
        //List
        IQueryable<T> List(Expression<Func<T, bool>> expression);
        //Find
        T Find(Guid id);
        //Delete
        void Delete(T entity);
        void Delete(Guid id);
        //Update
        void Update(T entity);
        //Patch
        void Patch(T entity, Guid id);
    }
}
